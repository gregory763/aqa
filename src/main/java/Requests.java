import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class Requests {
public static Double getRate(String url, String currency1, String currency2) {
    String requestUrl = url + "?expression=" + currency1 + "_in_" + currency2;

    final Content getResult;
    try {
        getResult = Request.Get(requestUrl).execute().returnContent();
    } catch (IOException e) {
        throw new RuntimeException(e);
    }

    // Считываем json
    Object obj = null; // Object obj = new JSONParser().parse(new FileReader("JSONExample.json"));
    try {
        obj = new JSONParser().parse(String.valueOf(getResult));
    } catch (ParseException e) {
        throw new RuntimeException(e);
    }
// Кастим obj в JSONObject
    JSONObject jo = (JSONObject) obj;
// Достаём firstName and lastName
    Double actualRate = (Double) jo.get("rate");


    System.out.println(actualRate);
    return actualRate;
}
    public static String getRateBinance(String url, String symbol) {
        String requestUrlBinance = url + "?symbol=" + symbol;

        final Content getResultBinance;
        try {
            getResultBinance = Request.Get(requestUrlBinance).execute().returnContent();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Считываем json
        Object obj = null; // Object obj = new JSONParser().parse(new FileReader("JSONExample.json"));
        try {
            obj = new JSONParser().parse(String.valueOf(getResultBinance));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
// Кастим obj в JSONObject
        JSONObject jo = (JSONObject) obj;
// Достаём firstName and lastName
        String actualRateB = (String) jo.get("price");


        System.out.println(actualRateB);
        return actualRateB;

}
}