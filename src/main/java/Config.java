public class Config {

    String url1 = "https://front.koshelek.ru/v2/p2p/offers/price";
    String currency11 = "BTC";
    String currency21 = "USD";

    String request1 = url1 + "?expression=" + currency11 + "_in_" + currency21;

    public String getRequest() {
        return request1;
    }

    public String getUrl() {
        return url1;
    }

    public void setUrl(String url) {
        this.url1 = url;
    }

    public String getCurrency1() {
        return currency11;
    }

    public void setCurrency1(String currency1) {
        this.currency11 = currency1;
    }

    public String getCurrency2() {
        return currency21;
    }

    public void setCurrency2(String currency2) {
        this.currency21 = currency2;
    }


}
